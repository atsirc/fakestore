import {ErrorBoundary} from 'react-error-boundary'
import Nav from '../components/Nav.js';
import ErrorMessage from '../components/Error.js'

const ErrorFallback = ({error}) =>{
  const ErrorComponent = <ErrorMessage content={error}/>
    return (<SingleView component={ErrorComponent}/>);
}

const myErrorHandler = (error, info) => {
  console.log(error)
}

const SingleView = ({component=null}) => {

  return (
    <ErrorBoundary FallbackComponent={ErrorFallback} onError={myErrorHandler}>
      <Nav />
      <div className="singleContainer">
          {component}
      </div>
    </ErrorBoundary>
    )
}

export default SingleView;
