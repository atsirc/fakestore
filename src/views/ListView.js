import {ErrorBoundary} from 'react-error-boundary'
import Categories from '../components/Categories.js';
import Nav from '../components/Nav.js';
import ErrorMessage from '../components/Error.js'
import SinglView from './SingleView.js';

const ErrorFallback = ({error}) =>{
  const ErrorComponent = <ErrorMessage content={error}/>
    return (<SinglView component={ErrorComponent}/>);
}

const myErrorHandler = (error, info) => {
  console.log(error)
}

const ListView = ({component=null}) => {

  return (
    <ErrorBoundary FallbackComponent={ErrorFallback} onError={myErrorHandler}>
      <Nav />
      <div className="container">
        <aside>
          <Categories/>
        </aside>
        <div className="content">
          {component}
        </div>
      </div>
    </ErrorBoundary>
    )
}

export default ListView;
