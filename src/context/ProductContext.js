import {useState, useEffect, createContext} from 'react';
import { getAllProducts } from '../services/productService.js';

export const ProductContext = createContext([[], () => []]);

export const ProductProvider = (props) => {
  const [products, setProducts] = useState([]);

  // I don't really think it is such a good idea to have the a fetch inside context, it seems to make error handling harder
  useEffect(() => {
    const getProducts = async () => {
      const products = await getAllProducts();
      setProducts(products);
    }
    getProducts();
  }, []);

  return (
    <ProductContext.Provider value={[products, setProducts]}>
      {props.children}
    </ProductContext.Provider>
  )
}
