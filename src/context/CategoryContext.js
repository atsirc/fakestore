import { useState, useEffect, createContext } from 'react';
import { getAllCategories } from '../services/categoryService.js';

export const CategoryContext = createContext([{}, () => {}]);

export const CategoryProvider = (props) => {

  const [categories, setCategories] = useState([]);
  useEffect(() => {
    const getCategories = async () => {
      const categories = await getAllCategories();
      setCategories(categories);
    }
    getCategories();
  }, []);

  return (
    <CategoryContext.Provider value={[categories, setCategories]}>
      {props.children}
    </CategoryContext.Provider>
  )
}
