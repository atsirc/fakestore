import axios from 'axios';

const allCategories = 'https://fakestoreapi.com/products/categories';
const singleCategory = 'https://fakestoreapi.com/products/category';

export const getAllCategories = async () => {
  try {
    const result = await axios.get(allCategories);
    console.log(result)
    return result.data;
  } catch (error) {
    return error;
  }
}

export const getProductsInCategory = async (id) => {
  try {
    const result = await axios.get(singleCategory + '/' + id);
    console.log(result)
    return result;
  } catch (error) {
    return error
  }
}
