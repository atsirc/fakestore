import axios from 'axios';

const baseUrl = 'https://fakestoreapi.com/products';

export const getAllProducts = async () => {
  try {
    const result = await axios.get(baseUrl);
    console.log(result)
    return result.data;
  } catch (error) {
    throw error;
  }
}

export const getProduct = async (id) => {
  try {
    const result = await axios.get(baseUrl + '/' + id);
    console.log(result)
    return result;
  } catch (error) {
    throw error;
  }
}

export const addProduct = async (product) => {
  try {
    const result = await axios.post(baseUrl, product);
    return result.data;
  } catch (error) {
    throw error;
  }
}

export const updateProduct = async (id, product) => {
  try {
    const result = await axios.put(baseUrl+'/'+id, product);
    return result.data;
  } catch (error) {
    throw error;
  }
}
