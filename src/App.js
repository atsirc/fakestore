import {
  Routes,
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import {ProductProvider} from './context/ProductContext.js';
import {CategoryProvider} from './context/CategoryContext.js';
import {ThemeProvider} from '@mui/material/styles';
import theme from './mui-theme.js';
import SingleView from './views/SingleView.js';
import ListView from './views/ListView.js';
import ErrorMessage from './components/Error.js';
import Home from './components/Home.js';
import Product from './components/Product.js';
import Products from './components/Products.js';
import Category from './components/Category.js';
import UpdateForm from './components/UpdateForm.js';
import './App.css';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
    <div className="App">
        <ProductProvider>
          <CategoryProvider>
              <Router>
                <Routes>
                  <Route path="/" element={<SingleView component={<Home />}/>} />
                  <Route path='/products' element={<ListView component={<Products/>}/>}></Route>
                  <Route path='/products/category/:id' element={<ListView component={<Category/>}/>}></Route>
                  <Route path='/products/categories' element={<ListView component={<Category/>}/>}></Route>
                  <Route path='/products/:id' element={<ListView component={<Product/>}/>}></Route>
                  <Route path='/products/update/:id' element={<SingleView component={<UpdateForm/>}/>}></Route>
                  <Route path='/products/new' element={<SingleView component={<UpdateForm/>}/>}></Route>
                  <Route path='/error' element={<SingleView component={<ErrorMessage/>}/>}></Route>
                </Routes>
              </Router>
            </CategoryProvider>
          </ProductProvider>
    </div></ThemeProvider>
  );
}

export default App;
