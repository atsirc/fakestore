import { useParams, } from 'react-router-dom';
import { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ProductContext } from '../context/ProductContext.js';
import Loading from './Loading.js';

const Product = () => {
  const [products, setProducts] = useContext(ProductContext)
  const params = useParams()
  const id = Number(params.id);
  const [product, setProduct] = useState(products.find(p => p.id === id));

  //for instances when reloading page
  useEffect(()=> {
    if (products.length !== 0) {
      setProduct(products.find(p => p.id === id))
    }
  }, [products])

  const productDescription = () => {
    return (
    <div className="product">
      <img src={product.image} alt='product'/>
      <div className="text">
        <span className="product-category">{product.category}</span>
        <h2 class="product-title">{product.title} </h2>
        <span className="product-price">{product.price} €</span>
        <p className="product-description">{product.description}</p>
        <Link to={'/products/update/'+id} class="update-product-link">Make changes</Link>
      </div>
    </div>
    )
  }


  return (
    <>
      {Object(product).hasOwnProperty('title') ? productDescription(): <Loading/>}
    </>
   )
}

export default Product;
