import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <div>
      <Link className="big-link" to="/products">View all products</Link>
      <Link className="big-link" to="/products/categories">View all categories</Link>
      <Link className="big-link" to="/products/new">Add product</Link>
    </div>
  )
}

export default Home;
