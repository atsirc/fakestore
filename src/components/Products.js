import { Link } from 'react-router-dom';
import { useContext } from 'react';
import { Box, ImageList, ImageListItem, ImageListItemBar} from '@mui/material';
import { ProductContext } from '../context/ProductContext.js';
import Loading from './Loading.js';

const Products = () => {
  const [products, setProducts] = useContext(ProductContext);

  const listProducts = () => {
    return (
      <>
        <h1>All products</h1>
        <Box>
          <ImageList cols={3} gap={7}>
              {
                products.map(product => (
                  <Link key={"link"+product.id} to={'/products/'+ product.id}>
                    <ImageListItem key={product.image}>
                      <img src={product.image} alt={product.title} loading="lazy" />
                      <ImageListItemBar position="below" title={`${product.title} ${product.price}`} />
                    </ImageListItem>
                  </Link>
                ))
              }
          </ImageList>
        </Box>
      </>
    );
  };

  return (
    <>
      { products.length === 0 ? <Loading/> : listProducts()}
    </>
  )
}

export default Products;
