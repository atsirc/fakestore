import { Link } from 'react-router-dom';
import { Alert } from '@mui/material';

const Nav = () => {
  return (
    <>
    <nav>
      <Link to='/'>Home</Link>
      <Link to='/products'>View all products</Link>
      <Link to='/products/categories'>View all categories</Link>
      <Link to='/products/new'>Add new product</Link>
    </nav>
    <Alert severity="warning">
      This page is for testing React Context, therefore all changes to items will be lost with reload. Category searches don't reflect changes
    </Alert>
    </>
  )
}

export default Nav;
