import { useContext } from 'react';
import { Link } from 'react-router-dom';
import { CategoryContext} from '../context/CategoryContext.js';
import Loading from './Loading.js';

const Categories = () => {
  const [categories, setCategories] = useContext(CategoryContext);

  const listCategories = () => {
    return (
      <>
        <h2>Categories</h2>
        <ul>
          {categories.map((category, idx) => 
          <Link class="category-link" key={'clink'+idx} to={"/products/category/" + category}>
            <li key={'category' + idx}>
              {category}
            </li>
          </Link>
          )}
        </ul>
      </>
    )
  };

  return (
    <>
      {categories.length === 0 ? <Loading/> : listCategories()}
    </>
  );
}

export default Categories;
