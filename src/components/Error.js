import Nav from './Nav.js';

const ErrorMessage = (props) => {
  console.log('inside errorview: ',  props)

  return (
    <>
      <div class="content">
        <p> We are sorry to inform you that our service encountered an error while handling your request.</p>
        <p> Error message: {props.content ? props.content.message : ''}</p>
      </div>
    </>
    )
}

export default ErrorMessage;
