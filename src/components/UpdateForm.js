import {useState, useEffect, useContext} from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { Button, InputLabel, Select, MenuItem, Divider } from '@mui/material';
import { ProductContext } from '../context/ProductContext.js';
import { CategoryContext } from '../context/CategoryContext.js';
import { addProduct, updateProduct } from '../services/productService';

const UpdateForm = () => {
  const params = useParams();
  const [id, setId] = useState(Number(params.id));
  const [isNew, setIsNew] = useState(false);
  const [products, setProducts] = useContext(ProductContext);
  const [categories, setCategories] = useContext(CategoryContext);
  const [product, setProduct] = useState({});
  const navigate = useNavigate();

  useEffect(()=> {
    const productToBeUpdated = products.find(p => p.id === id);
     if (window.location.pathname.includes('new')){
      console.log('no product found')
      const newId = products.length + 1;
      setIsNew(true);
      setProduct({
        title: '',
        description: '',
        price: '',
        category: 'electronics'
      });
      setId(newId);
     } else if (productToBeUpdated) {
        console.log('product found')
        setProduct(productToBeUpdated);
      }}, [params]);

  const applyChanges = async (event) => {
    event.preventDefault();
    try {
      if (!isNew) {
          const updatedProduct = await updateProduct(id, product);
          // updatedProduct.id has to be changed because otherwise they would overrun each other
          updatedProduct.id = id;
          setProducts(oldProducts => oldProducts.map(product => product.id === id ? updatedProduct: product));
        } else {
          // this image thing is because got some kind of payload overload issue...
          const img = product.image;
          product.image = '';
          const newProduct = await addProduct(product);
          newProduct.id = id;
          newProduct.image = img;
          setProducts( prev => (prev.concat(newProduct)));
        }
        navigate('/products/'+id);
      } catch (error) {
      navigate('/error');
    }
    
  }

  const updateValue = ({target}) => {
    switch (target.name) {
      case 'title' :
        setProduct({...product, title: target.value});
        break;
      case 'price':
        setProduct({...product, price: target.value});
        break;
      case 'category':
        setProduct({...product, category: target.value});
        break;
      case 'newCategory':
        setProduct({...product, category: target.value});
        break;
      case 'description':
        setProduct({...product, description: target.value});
        break;
      case 'upload-image':
        console.log(target.value);
        const reader = new FileReader();
        reader.addEventListener('load', ()=> {
          setProduct({...product, image: reader.result})
        });
        reader.readAsDataURL(target.files[0]);
        break;
      default:
        console.log('fail')
        break;
    }
  }

  return (
    <div className="form-container">
      <ValidatorForm onSubmit={applyChanges} onError={error => console.log(error)}>
        <TextValidator className="TextValidator" id="title" label="title" onChange={updateValue} name="title" value={product.title || ''} validators={['required',`matchRegexp:^[\\w\\W]{3,}$`]} errorMessages={['this field is required', 'at least 3 charachters are required']} fullWidth/>
        <TextValidator className="TextValidator" id="price" label="price" onChange={updateValue} name="price" value={product.price || ''} validators={ ['required', 'minNumber:0', 'matchRegexp:^\\d+([.,]\\d{2})?$']} errorMessages={['this field is required', 'Has to be a positive number. , and . can be used for indicating decimals']} fullWidth/>
        <InputLabel id="label-for-select">Category</InputLabel>
        <Select labelId="label-for-select" label="Category" name="category" id="category" value={product.category || ''} onChange={updateValue}>
            {
              categories.length > 0 && categories.map(category => <MenuItem key={'option'+category} value={category} >{category}</MenuItem>)
            }
            <MenuItem value="newCategory" key={'optionnewCategory'}>Add a new category</MenuItem>
          </Select>
          {
            !categories.includes(product.category) ? 
              <TextValidator label="newCategory" onChange={updateValue} name="newCategory"/>
              : ''
          }

          <TextValidator className="TextValidator" multiline rows="7" id="description" label="description" onChange={updateValue} name="description" value={product.description || ''} validators={ ['required']} errorMessages={['this field is required']} fullWidth/>
          <label htmlFor="upload-image">
            <input style={{ display: 'none'}} onChange={updateValue} id="upload-image" name="upload-image" type="file"/>
            <Button color="secondary" variant="contained" component="span">
              Upload image
            </Button>
          </label>
          <Divider/>
          <Button variant="contained" type="submit">Submit</Button>
      </ValidatorForm>
    </div>
  )
}

export default UpdateForm;
