import { Link, useParams, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { getProductsInCategory } from '../services/categoryService.js';
import Loading from './Loading';

const Category = () => {
  const [products, setProducts] = useState([])
  const params = useParams()
  const [id, setId] = useState(params.id)
  const navigate = useNavigate();

  //for instances when reloading page
  useEffect(()=> {
    if (typeof id === 'undefined') {
       navigate('/products/category/electronics');
    }
    const getProducts = async (id) => {
      // setter to show Loading
      setProducts([])
      const result = await getProductsInCategory(id);
      if (result.data.length > 0) {
        setProducts(result.data)
      }
    }
    getProducts(id);
  }, [id]);

  useEffect(()=> {
    setId(params.id);
  }, [params])

  // TODO find out how to not need this.. It's basically the same as in Products...
  const listProducts = () => {
    return (
      <>
      <h2>{id}</h2>
        <ul className="product-grid">
          {products.map(p => 
          <Link key={'link'+p.id} to={'/products/'+p.id}>
            <li key={p.id}>
              <img src={p.image} alt="product"/>
              <span class="product-presentation">{p.title} {p.price}€</span>
            </li>
          </Link>
          )}
        </ul>
      </>
    )
  }

  return (
    <>
      {products.length > 0 ? listProducts(): <Loading />}
    </>
   )
}

export default Category;
