import { createTheme } from '@mui/material/styles';

export default createTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#6371b7',
    },
    secondary: {
      main: '#d03b70',
    },
  },
  spacing: 8,
  margin: '20px',
  Button: {
    root: {
      margin: "10px",
      padding: "10px"
    }
  },
  components: {

    MuiImageListItem: {
      styleOverrides: {
        root: {
          maxWidth: '23vw'
        }
      }
    },
    MuiImageList: {
      styleOverrides: {
        root: {
          width: '80vw'
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        // Name of the slot
        root: {
          // Some CSS
          marginTop: '1rem',
          padding: '.5rem 2rem'

        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          marginTop: '1rem'
        }
      }
    },
    MuiDivider: {
      styleOverrides: {
        root: {
          marginTop: '1rem',
          marginBottom: '.0rem'
        }
      }
    },
    MuiSelect: {
      styleOverrides: {
        root: {
          padding: '1rem'
        }
      }
    
    },
    MuiAlert: {
      styleOverrides: {
        root: {
          alignItem: 'center'
        },
        icon: {
        },
        message: {
        }
      }
    }
  }
});