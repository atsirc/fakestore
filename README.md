# Trying out React'c Context hook
This is a React-app frontend using fakestore.com's api and React's context hook. Therefore some changes are presistent until reload and some changes don't seem to be saved.  
If you make changes to or add a new product, this will show upp in 'View all products', but if you add a new category to a product this will not show up in the 'View categories'. This is because the products are just fetched once, while categories are fetched by category.

